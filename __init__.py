# ##### BEGIN GPL LICENSE BLOCK #####
# 
#   __init__.py
#
#
#   Panel in scene properties for VCS(Version Control System) Add.
#
# Copyright 2014 gumbi <pracacp@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>

bl_info = {
    "name": "Menu Git and Mercurial ",
    "author": "gumbicp",
    "version": (1, 1, 0),
    "blender": (2, 65, 11),
    "location": "Scene Properties",
    "description": "Version Control System Menu",
    "warning": "Not done yet !!!",
    "wiki_url": "https://bitbucket.org/gumbicp/vcs",
    "category": "VCS"}

import bpy
# ImportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ImportHelper
from bpy_extras.io_utils import ExportHelper
from bpy.props import *
from bpy.types import Operator
from subprocess import CalledProcessError
from .vcsproperty import VcsProperty
from . import vcsglobals
import os


def _set_vcs_module_object_path(self, filepath):
    """
        Function import vcsmanager and set global variable vcs_path.
        
        @param self: for function report()
        @param filepath: to repository
        @return True: or None if Import Error occurred
    """
    try:
        if not bpy.context.scene.vcs_clone:
            if os.access(filepath, os.F_OK):
                bpy.ops.wm.open_mainfile(filepath=filepath)

        from . import vcsmanager
        # we have to separate path and file if exist
        file_path = bpy.utils._os.path.split(str(filepath))
        bpy.context.scene.vcs_path = file_path[0]
        vcsglobals.set_property_values(vcs_module=vcsmanager)
        return True
    # error origin from import
    except ImportError as e:
        _info_area_report_debug(self, "In Try: except: ", [e], line="code line 82", info={'WARNING'})
        vcsglobals.set_property_values(vcs_error=True)
        return None


def _set_variables(self, context, filepath):
    """
        Function set variables :
            - path to repository
            - remember last old path for cancel options
            - set object repository class as vcs_module object
        @param context: object context
        @param filepath: path to repository folder
    """
    # jeżeli repo istnieje to ustawimy zmienne jakiego typu jest repo
    # ustawiamy ścieźkę do niego
    if _set_vcs_module_object_path(self, filepath):
        repo = context.scene
        repo.vcs_repo = 'git'
        try:
            # object for git commands parser
            vcsglobals.set_property_values(vcs_module_object=
                                           vcsglobals.get_property_value('vcs_module').
                                           GITManager(repo.vcs_path))
            # we want remember old path from render
            if vcsglobals.get_property_value('vcs_u_path_from_r_t'):
                vcsglobals.set_property_values(vcs_user_path_from_render=repo.render.filepath,
                                               vcs_u_path_from_r_t=False)
                repo.render.filepath = repo.vcs_path
        except ValueError as e:
            self.report({'WARNING'}, "Error for more info see terminal")
            print(e)
            repo.vcs_repo = ''
            vcsglobals.set_property_values(vcs_error=True)

    return {'FINISHED'}


def _check_blend_extension(file):
    """
    Function check file extension - >.blend
    
     @return True: if extension of file is .blend 
     @return False: if extension of file is not .blend
     @param file: file to check
     @raise AttributeError: if file is not instance of string
    """
    if not isinstance(file, str):
        raise AttributeError

    (name, ext) = os.path.splitext(file)
    if ext == '.blend':
        return True
    else:
        return False


def _set_blend_from_clone_path(path):
    """
    Function look for file.blend in clone path,
    set list of blend files from clone path.
    
    @return: First index of blend files 
            or None if there is not any .
    """
    indexblendsext = None
    tabblendfiles = []
    
    for root, dirs, files in os.walk(path):
        # na start drukujemy katalog ,
        #ile bajtów zajmują pliki w katalogu i ile ich jest

        print(root, "consumes", end=" ")
        print(sum(os.path.getsize(os.path.join(root, name)) for name in files), end=" ")
        print("bytes in ", len(files), " non-directory files")

        indexblendsext = [blendfile for blendfile in files if _check_blend_extension(blendfile)]
        [tabblendfiles.append(os.path.join(root, file)) for file in indexblendsext]

    if len(tabblendfiles) is not 0:
        return tabblendfiles[0]
    else:
        return None


def _set_repository_clone(self, context, filepath):
    """
    Function clone to folder repository . Set and open blend file if any.
    May raise few exceptions (CalledProcessError, ValueError, AttributeError)

    @see _set_vcs_module_object_path():
    @see vcsmanager.py -> clone(http, path):
    @see _set_blend_from_clone_path():

    """

    def clear():
        """ Just in case exception , clear variables """
        vcsglobals.set_property_values(vcs_error=True)
        context.scene.vcs_clone = False
        context.scene.vcs_repo = ''
        context.scene.vcs_http = ''
        context.scene.vcs_path = ''
        bpy.context.scene.vcs_path_blend_file = ''

        if not vcsglobals.get_property_value('vcs_u_path_from_r_t'):
            context.scene.render.filepath =\
                '{}'.format(vcsglobals.get_property_value('vcs_user_path_from_render'))
            vcsglobals.set_property_values(vcs_user_path_from_render='', vcs_u_path_from_r_t=True)
        ############# End clean() method #############################################################

    if _set_vcs_module_object_path(self, filepath):
        output = None
        try:
            output = vcsglobals.get_property_value('vcs_module').\
                clone(context.scene.vcs_http, context.scene.vcs_path)
            output = output.decode()
            self.report({'INFO'}, output)

            # object for git commands parser and we want remember old path from render
            vcsglobals.set_property_values(
                vcs_module_object=vcsglobals.get_property_value('vcs_module').
                GITManager(context.scene.vcs_path),
                vcs_user_path_from_render=context.scene.render.filepath,
                vcs_u_path_from_r_t=False
            )
            #
            blendfile = _set_blend_from_clone_path(context.scene.vcs_path)
            if blendfile:
                bpy.ops.wm.open_mainfile(filepath=blendfile)
                
            bpy.context.scene.render.filepath = context.scene.vcs_path
            context.scene.vcs_repo = 'git'
            context.scene.vcs_clone = True
            context.scene.vcs_http = self.my_string_http
            vcsglobals.set_property_values(vcs_error=False)
        # errors
        except CalledProcessError as e:
            self.report({"ERROR"}, "Error")
            self.report({'WARNING'},
                        "Wrong repo address or repo exist in this folder see terminal for more info")
            clear()
            return {'FINISHED'}
        except ValueError as e:
            self.report({"ERROR"}, "Error")
            self.report({'WARNING'},
                        "Wrong repo address or repo exist in this folder see terminal for more info")
            clear()
            return {'FINISHED'}

    self.report({'INFO'}, "Cloning done.")
    return {'FINISHED'}
################### END Variable Setter Functions ######################


################### Utility Functions ##########################
def _info_area_report_debug(self, str_info, tab, line=None, info={'INFO'}):
    """
    Function just for debug purpose.
    @param self: object from class extended by operators
    @param str_info: debug info from programmer
    @param tab: [variables] to show on Info panel in Blender
    @param line: code line number
    @param info:  {‘DEBUG’, ‘INFO’, ‘OPERATOR’, ‘PROPERTY’, ‘WARNING’, ‘ERROR’,
                   ‘ERROR_INVALID_INPUT’, ‘ERROR_INVALID_CONTEXT’, ‘ERROR_OUT_OF_MEMORY’}
                   @see : http://www.blender.org/documentation/blender_python_api_2_69_release/
                                bpy.types.Operator.html#bpy.types.Operator
    """
    self.report(info, '#'*50)
    for v in tab:
        self.report(info, str_info + " -> variable :{0}, ={1}".format(type(v), v))
    self.report(info, '-'*50)
    if line:
        self.report(info, line)
        self.report(info, '#'*50)
################### END Factory Functions ######################


################################################################
################### Classes Blender API ########################
################################################################
class SetRepositoryPaht(Operator, ImportHelper):
    """  Class sets repository path using blender file manager"""
    bl_idname = "export.repo_path"
    bl_label = "Export Repository Path"

    def execute(self, context):
        """
        If we have to initialize some variables, it is done before we call
        Function _set_variables(). That way you can set some options in blender file
        manager on the right down side window.
        """
        return _set_variables(self, context, self.filepath)


class Panel_scene_vcs(bpy.types.Panel):
    """ Class main menu For Version Control System """

    # def __init__(self):
    #     VcsProperty.__init__(self)
    #     pass

    bl_label = "VCS Git Project"
    bl_idname = "SCENE_PT_layout_vcs_scene_panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    @classmethod
    def poll(cls, context):
        return context.scene.vcs_repo is not None

    def draw(self, context):
        """
            Method draw menu on screen. First step find repository path,
            draw just VCS Path in menu.
            Second step if repository is set draw menu with commands .
        """
        scene = context.scene
        repo = scene.vcs_repo
        ##
        #   Start menu
        #
        if len(repo) == 0:
            #label and menu item Vcs Repository Path
            self.layout.operator_context = 'INVOKE_DEFAULT'
            self.layout.label(text="Project repository from: ")
            self.layout.operator(WM_OT_Panel_scene_vcs_init.bl_idname, text="New Project")
            self.layout.operator(SetRepositoryPaht.bl_idname, text="Load Local Project")
            # menu item Vcs Clone from http
            self.layout.operator(WM_OT_Panel_scene_vcs_clone.bl_idname, text="Get Project from http")
            self.layout.operator(WM_OT_Panel_scene_vcs_help_main_menu.bl_idname, text="Help, about VCS concept")
            if vcsglobals.get_property_value('vcs_error'):
                self.layout.label(text='ERROR: There is not valid git repository loaded!!!')
        ##
        # elif for cloning panel options
        #

        else:
            ##
            #   save menu
            #
            if vcsglobals.get_property_value('vcs_error'):
                vcsglobals.set_property_values(vcs_error=False)

            layout = self.layout
            ### Git repository menu ----------------------------
            if repo == "git":
                layout.label(text="Git Project")
            #---------------------------------------------------
            if bpy.context.scene.vcs_clone:
                # labels
                layout.label(text='Project from :')
                layout.label(text='{}'.format(bpy.context.scene.vcs_http))
                # render context
                rd = scene.render
                # toggle to remember old path
                if vcsglobals.get_property_value('vcs_u_path_from_r_t'):
                    print('{}'.format(rd.filepath))
                    vcsglobals.set_property_values(vcs_user_path_from_render=rd.filepath,
                                                   vcs_u_path_from_r_t=False)

            # END clone menu --------------------------
            layout.operator(WM_OT_Panel_scene_vcs_help_save_menu.bl_idname, text="About")
            layout.label(text="Save Menu")
            row = layout.row()
            row.operator(WM_OT_Panel_scene_vcs_save.bl_idname, text="Save")
            row.operator(WM_OT_Panel_scene_vcs_push_save_menu.bl_idname, text="Push")
            row.operator(WM_OT_Panel_scene_vcs_cancel.bl_idname, text="Cancel")

            # END save menu ------------------------------------


class WM_OT_Panel_scene_vcs_init(bpy.types.Operator):
    """ Init Git Project"""
    bl_label = "New git repository"
    bl_idname = "object.vcs_init"

    def execute(self, context):
        self.report({'INFO'}, "Init project done")
        return {'FINISHED'}


class WM_OT_Panel_scene_vcs_clone(bpy.types.Operator, ImportHelper):
    """ Clone Dialog sets url to repository on the web """
    bl_label = "Clone Repository from http"
    bl_idname = "object.vcs_clone"
    my_string_http = bpy.props.StringProperty(name=" HTTP:// ", default='')
    #my_bool_is_http_set = bpy.props.BoolProperty(name="Set", default=True)
    filepath = StringProperty(
        name="File Path",
        description="Filepath used for importing the file",
        maxlen=1024,
        subtype='FILE_PATH',)

    def execute(self, context):

        wm = context.window_manager
        bpy.context.scene.vcs_http = self.my_string_http
        context.scene.vcs_clone = True
        _set_repository_clone(self, context, self.filepath)

        return {'FINISHED'}


class WM_OT_Panel_scene_vcs_cancel(bpy.types.Operator):
    """ Operator Reset Global Variables for repository. """
    bl_label = "Reset global variables"
    bl_idname = "object.vcs_cancel"

    def execute(self, context):
        """
        Reset global variables.
        @param context: bpy.context
        @return: {'FINISHED'}
        """
        context.scene.vcs_repo = ''
        context.scene.vcs_http = ''
        context.scene.vcs_path = ''
        context.scene.vcs_clone = False
        bpy.context.scene.vcs_path_blend_file = ''

        vcsglobals.set_property_values(vcs_error=False, vcs_module_object=None, vcs_module=None)
        # switch between remembered path from user settings
        # before we cancel cloning but we just set new path
        # this way we got old one set back

        if not vcsglobals.get_property_value('vcs_u_path_from_r_t'):
            
            bpy.context.scene.render.filepath = '{}'.format(vcsglobals.get_property_value('vcs_user_path_from_render'))
            vcsglobals.set_property_values(vcs_user_path_from_render='', vcs_u_path_from_r_t=True)

        self.report({'INFO'}, 'Cancel from Vcs')

        return {'FINISHED'}


class WM_OT_Panel_scene_vcs_save(bpy.types.Operator, ExportHelper):
    """ Save Blend file and git add/commit. """
    bl_label = "Save project"
    bl_idname = "object.vcs_save"
    bl_info = "Blend file and git add/commit/push"
    vcs_author = bpy.props.StringProperty(name="author: ", default="")
    vcs_email = bpy.props.StringProperty(name="email: ", default="")
    filename_ext = '.blend'
    filepath = StringProperty(
        name="File Path",
        description="Filepath used for exporting the file",
        maxlen=1024,
        subtype='FILE_PATH',)

    def check(self, context):
        self.filepath = os.path.join(bpy.context.scene.vcs_path, '{}'.format(self.filepath))

        return self.filepath

    def execute(self, context):
        """
        @return: {'FINISHED'}
        """
        context.scene.vcs_path_blend_file = self.filepath
        bpy.ops.wm.save_as_mainfile(filepath=self.filepath)
        self.report({'INFO'}, self.filepath)
        self.report({'INFO'}, 'Projects save done')

        return {'FINISHED'}


class WM_OT_Panel_scene_vcs_help_save_menu(bpy.types.Operator):
    """ Operator Help About Repository Save. """
    bl_label = "Help About Repository Save"
    bl_idname = "object.vcs_help_save"

    def execute(self, context):
        """
        @return: {'FINISHED'}
        """
        self.report({'INFO'}, 'Vcs Help in Text Editor')

        return {'FINISHED'}


class WM_OT_Panel_scene_vcs_push_save_menu(bpy.types.Operator):
    """ Push Project to Server. """
    bl_label = "Push project to server"
    bl_idname = "object.vcs_push_save_menu"

    def execute(self, context):
        """
        @return: {'FINISHED'}
        """
        self.report({'INFO'}, 'Push done')

        return {'FINISHED'}


class WM_OT_Panel_scene_vcs_help_main_menu(bpy.types.Operator):
    """ Operator Help About Repository Main. """
    bl_label = "Help About Repository Main"
    bl_idname = "object.vcs_help_main"

    def execute(self, context):
        """
        @return: {'FINISHED'}
        """
        self.report({'INFO'}, 'Vcs Help in Text Editor')

        return {'FINISHED'}
################### END Classes Blender API ######################


### REGISTER CLASS END FUNCTIONS IN BLENDER API ##
def register():
    bpy.utils.register_module(__name__)


def unregister():

    vcsglobals.set_property_values(vcs_module_object=None, vcs_error=False, vcs_module=None)
    bpy.context.scene.vcs_repo = ''
    bpy.context.scene.vcs_http = ''
    bpy.context.scene.vcs_path = ''
    bpy.context.scene.vcs_clone = False
    bpy.context.scene.vcs_path_blend_file = ''

    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()

#TODO: zastanowić się które komendy są nam potrzebne ?
#TODO: skończyć parsowanie komend w vcsmanager
#TODO: do klonowania lub tworzenia swojego z opcji dokończyć
#TODO: dodać autora  --author=bpy.context.user_preferences.system.author do flagi commit