#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  vcsconstant.py
#
#
#
# Copyright 2014 gumbi <pracacp@gmaill.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
"""
###########################################################################################################
################################################# Git #####################################################
# add                 diff                rebase
# am                  fetch               reflog
# annotate            filter-branch       relink
# apply               format-patch        remote
# archive             fsck                repack
# bisect              gc                  replace
# blame               get-tar-commit-id   request-pull
# branch              grep                reset
# bundle              gui                 revert
# checkout            help                rm
# cherry-pick         imap-send           send-email
# cherry              init                shortlog
# citool              instaweb            show-branch
# clean               log                 show
# clone               mergetool           stage
# commit              merge               stash
# config              mv                  status
# credential-cache    name-rev            submodule
# credential-store    notes               svn
# describe            pull                tag
# difftool            push                whatchanged
###########################################################################################################
"""
# ----------------------------------------------  GIT HELP FLAGS ------------------------------------------
#SET_HELP_FLAGS = {'-a', '--all', '-i', '--info', '-m,', '--man', '-w', '--web'}

# ----------------------------------------------  GIT ADD FLAGS ------------------------------------------
SET_ADD_FLAGS = {'-n', '--dry-run', '--verbose', '-v', '--force', '-f', '--interactive', '-i', '--patch', '-p',
                 '--edit', '-e', '-A', '--all', '--update', '-u', '--intent-to-add', '-N',
                 '--refresh', '--ignore-errors', '--ignore-missing', '--'}
#TODO implement SeTs
#SET_DIFF_FLAGS = {'--raw', '--minimal', '--histogram'}
# ----------------------------------------------  GIT STATUS FLAGS ------------------------------------------
#@TODO implement -u[mode]
SET_STATUS_FLAGS = {'-s', '--short', '-b', '-branch', '--porcelain', '--ignored', '-z'}
# ----------------------------------------------  GIT COMMIT FLAGS ------------------------------------------
#@TODO finish commit
SET_COMMIT_FLAGS = {'-a', '--all', '-p', '--patch', '--short', '--porcelain', '-z', '-m', '--message', '-s',
                    '--signoff', '-n', '--no-verify', '--allow-empty', '--allow-empty-message', '--amend', '-v',
                    '--verbose', '-q', '--quiet', '--dry-run', '--status', '--no-status'}
#SET_REBASE_FLAGS = {}
#SET_AM_FLAGS = {}
SET_FETCH_FLAGS = {}
#SET_REFLOG_FLAGS = {}
#SET_ANNOTATE_FLAGS = {}
#SET_FILTER_BRANCH_FLAGS = {}
#SET_RELINK_FLAGS = {}
#SET_APPLY_FLAGS = {}
#SET_FORMAT_PATCH_FLAGS = {}
SET_REMOTE_FLAGS = {}
#SET_ARCHIVE_FLAGS = {}
#SET_FSCK_FLAGS = {}
#SET_REPACK_FLAGS = {}
#SET_BISECT_FLAGS = {}
#SET_GC_FLAGS = {}
#SET_REPLACE_FLAGS = {}
#SET_BLAME_FLAGS = {}
#SET_GET_TAR_COMMIT_ID_FLAGS = {}
#SET_REQUEST_PULL_FLAGS = {}
SET_BRANCH_FLAGS = {}
#SET_GREP_FLAGS = {}
SET_RESET_FLAGS = {}
#SET_BUNDLE_FLAGS = {}
SET_GUI_FLAGS = {}
#SET_REVERT_FLAGS = {}
SET_CHECKOUT_FLAGS = {}
SET_RM_FLAGS = {}
#SET_CHERRY_PICK_FLAGS = {}
#SET_IMAP_SEND_FLAGS = {}
#SET_SEND_EMAIL_FLAGS = {}
#SET_CHERRY_FLAGS = {}
SET_INIT_FLAGS = {}
SET_SHORTLOG_FLAGS = {}
#SET_CITOOL_FLAGS = {}
#SET_INSTAWEB_FLAGS = {}
SET_SHOW_BRANCH_FLAGS = {}
#SET_CLEAN_FLAGS = {}
# ----------------------------------------------  GIT LOG FLAGS ------------------------------------------
SET_LOG_FLAGS = {'--pretty=oneline', '--pretty=short', '--pretty=medium', '--pretty=full', '--pretty=fuller',
                 '--pretty=email', '--pretty=raw', '--abbrev-commit', '--tags', '--simplify-by-decoration', '--follow'}
SET_SHOW_FLAGS = {}
SET_CLONE_FLAGS = {}
#SET_MERGETOOL_FLAGS = {}
#SET_STAGE_FLAGS = {}
SET_MERGE_FLAGS = {}
#SET__STASH_FLAGS = {}
# ----------------------------------------------  GIT CONFIG FLAGS ------------------------------------------
SET_CONFIG_FLAGS = {'-l'}
SET_MV_FLAGS = {}
#SET_CREDENTIAL_CACHE_FLAGS = {}
#SET_NAME_REV_FLAGS = {}
#SET_SUBMODULE_FLAGS = {}
#SET_CREDENTIAL_STORE_FLAGS = {}
SET_NOTES_FLAGS = {}
#SET_SVN_FLAGS = {}
#SET_DESCRIBE_FLAGS = {}
SET_PULL_FLAGS = {}
SET_TAG_FLAGS = {}
#SET_DIFF_TOOL_FLAGS = {}
SET_PUSH_FLAGS = {}
#SET_WHATCHANGED_FLAGS = {}
# ----------------------------------------------  DICT FOR GIT COMMANDS ---------------------------------------
# COMMANDS_GIT = {'help': SET_HELP_FLAGS, 'add': SET_ADD_FLAGS, 'diff': SET_DIFF_FLAGS, 'status': SET_STATUS_FLAGS,
#                 'commit': SET_COMMIT_FLAGS, 'rebase': SET_REBASE_FLAGS, 'am': SET_AM_FLAGS, 'fetch': SET_FETCH_FLAGS,
#                 'reflog': SET_REFLOG_FLAGS, 'annotate': SET_ANNOTATE_FLAGS, 'filter-branch': SET_FILTER_BRANCH_FLAGS,
#                 'relink': SET_RELINK_FLAGS, 'apply': SET_APPLY_FLAGS, 'format-patch': SET_FORMAT_PATCH_FLAGS,
#                 'remote': SET_REMOTE_FLAGS, 'archive': SET_ARCHIVE_FLAGS, 'fsck': SET_FSCK_FLAGS,
#                 'repack': SET_REPACK_FLAGS, 'bisect': SET_BISECT_FLAGS, 'gc': SET_GC_FLAGS,
#                 'replace': SET_REPLACE_FLAGS, 'blame': SET_BLAME_FLAGS,
#                 'get-tar-commit-id': SET_GET_TAR_COMMIT_ID_FLAGS, 'request-pull': SET_REQUEST_PULL_FLAGS,
#                 'branch': SET_BRANCH_FLAGS, 'grep': SET_GREP_FLAGS, 'reset': SET_RESET_FLAGS,
#                 'bundle': SET_BUNDLE_FLAGS, 'gui': SET_GUI_FLAGS, 'revert': SET_REVERT_FLAGS,
#                 'checkout': SET_CHECKOUT_FLAGS, 'rm': SET_RM_FLAGS, 'cherry-pick': SET_CHERRY_PICK_FLAGS,
#                 'imap-send': SET_IMAP_SEND_FLAGS, 'send-email': SET_SEND_EMAIL_FLAGS, 'cherry': SET_CHERRY_FLAGS,
#                 'init': SET_INIT_FLAGS, 'shortlog': SET_SHORTLOG_FLAGS, 'citool': SET_CITOOL_FLAGS,
#                 'instaweb': SET_INSTAWEB_FLAGS, 'show-branch': SET_SHOW_BRANCH_FLAGS, 'clean': SET_CLEAN_FLAGS,
#                 'log': SET_LOG_FLAGS, 'show': SET_SHOW_FLAGS, 'clone': SET_CLONE_FLAGS,
#                 'mergetool': SET_MERGETOOL_FLAGS, 'stage': SET_STAGE_FLAGS, 'merge': SET_MERGE_FLAGS,
#                 'stash': SET__STASH_FLAGS, 'config': SET_CONFIG_FLAGS, 'mv': SET_MV_FLAGS,
#                 'credential-cache': SET_CREDENTIAL_CACHE_FLAGS, 'name-rev': SET_NAME_REV_FLAGS,
#                 'submodule': SET_SUBMODULE_FLAGS, 'credential-store': SET_CREDENTIAL_STORE_FLAGS,
#                 'notes': SET_NOTES_FLAGS, 'svn': SET_SVN_FLAGS, 'describe': SET_DESCRIBE_FLAGS, 'pull': SET_PULL_FLAGS,
#                 'tag': SET_TAG_FLAGS, 'difftool': SET_DIFF_TOOL_FLAGS, 'push': SET_PUSH_FLAGS,
#                 'whatchanged': SET_WHATCHANGED_FLAGS}
COMMANDS_GIT = {'add': SET_ADD_FLAGS, 'status': SET_STATUS_FLAGS,
                'commit': SET_COMMIT_FLAGS, 'fetch': SET_FETCH_FLAGS,
                'remote': SET_REMOTE_FLAGS, 'branch': SET_BRANCH_FLAGS, 'reset': SET_RESET_FLAGS,
                'gui': SET_GUI_FLAGS, 'checkout': SET_CHECKOUT_FLAGS, 'rm': SET_RM_FLAGS,
                'init': SET_INIT_FLAGS, 'shortlog': SET_SHORTLOG_FLAGS, 'show-branch': SET_SHOW_BRANCH_FLAGS,
                'log': SET_LOG_FLAGS, 'show': SET_SHOW_FLAGS, 'clone': SET_CLONE_FLAGS,
                'merge': SET_MERGE_FLAGS,
                'config': SET_CONFIG_FLAGS, 'mv': SET_MV_FLAGS,
                'notes': SET_NOTES_FLAGS, 'pull': SET_PULL_FLAGS,
                'tag': SET_TAG_FLAGS, 'push': SET_PUSH_FLAGS}
###########################################################################################################
################################################# Mercurial ###############################################
###########################################################################################################
