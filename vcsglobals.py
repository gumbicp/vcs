#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#   vcsglobals.py
#
#   File store global variables in dictionary. Two methods getter
# and setter.
#
# Copyright 2014 gumbi <pracacp@gmaill.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
__version__ = "beta.1.0.3"
__author__ = 'gumbicp'

global_variables = {'vcs_u_path_from_r_t': True,
                    'vcs_user_path_from_render': '',
                    'vcs_module': None,
                    'vcs_module_object': None,
                    'vcs_error': False}


def get_property_value(key):
    """
    Getter for global variables (dict).
    @param key: name variable
    @return: variable value
    """
    global global_variables
    return global_variables[key]


def set_property_values(**kwargs):
    """
    Setter for global variable. Key=Value.
    """
    global global_variables
    for key, value in kwargs.items():
        global_variables[key] = value