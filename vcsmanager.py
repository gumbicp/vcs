# -*- coding: utf-8 -*-
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  vcsmanager.py
#
#   Class wrapper for System Control Version. For git GITManager(Object)
#   VCSPaths set repository path.
#   Its not donne yet.
#
# Copyright 2014 gumbi <pracacp@gmaill.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
__author__ = "gumbicp"
__version__ = "beta.1.0.3"

from subprocess import check_output     # manage shell
from subprocess import CalledProcessError
try:
    # testing module purpose
    import vcspaths
    import vcsconstant

except ImportError:
    # for bpy in blender
    from . import vcspaths
    from . import vcsconstant


def clone(http, path):
    """
    Function clone repository using subprocess method check_output() .
    @return: output string from clone command
    """
    path = vcspaths.set_path(path, clone=True)
    output = None
    output = check_output(['git', 'clone', http])
    if output:
        vcspaths.set_path_for_clone(path, http)
        return output

    raise ValueError('Something go wrong in ', output)


class GITManager(object):
    """
        Git Manager Class
    """
    def __init__(self, path=None):
        vcspaths.set_path(path)
        self.commands = {}
        self.commands.update(vcsconstant.COMMANDS_GIT)
        self.git = 'git'

    def execute_command(self, command, **flags):
        """
        Method execute VSC GIT command
        @param command: to execute in VCS
        @param flags: options for command, but for filepattern flag must be fp=<filepattern>
        @return: bite string of output command or None
        """
        try:
            # we have parse flags , if there is one we gonna process command with flags
            # if not we got None type in this variable
            pars_flags_for_shell = self._pars_flags(command, flags)
            if __debug__:
                print('pars_flags_for_shell = ',
                      pars_flags_for_shell,
                      ' --- line 70 <class GITManager :: ->> execute_command() ')

            # if there is not flags for command
            if not pars_flags_for_shell:
                output = check_output([self.git, command])
            # for list of flags
            elif isinstance(pars_flags_for_shell, list):

                list_command_flags = [self.git, command]

                for value in pars_flags_for_shell:
                    list_command_flags.append(value)

                output = check_output(list_command_flags)
            # for one flag
            else:
                output = check_output([self.git, command, pars_flags_for_shell])

        except (CalledProcessError, KeyError) as e:
            # just in case when we got wrong commend to process with
            output = None

        return output

    def _case_log_follow(self, flag):
        """
        Method check out flag  log --follow file
        @param flag: to check
        @return: True or False
        """
        if flag == {'--follow'}:
            return True
        else:
            return False

    def _case_help(self, command, f_set):
        """
        Method check out command help . If command is help and set flags is in git commands,
        method will return True else if it`s not a case return false.
        @param command:
        @param f_set: set flags to check out (git commands)
        @return: True or False
        """
        if command == 'help' and [k for k in self.commands.keys() if f_set.__contains__(k)]:
            return True
        else:
            return False

    def _pars_flags(self, command, flags):
        """
        Method parse flags for commands GIT

        @param command: of the GIT
        @param flags: for processing, but for filepattern flag must be fp=<filepattern>
        @raise KeyError: if flags is`nt in command flags or filepattern (example: fp='*.*' or '*.py' ...)
        @return: flags or None
        """
        if flags:

            # pull from command all flags
            pars_flags_for_shell = [(com, fls) for com, fls in self.commands.items() if com == command]
            pars_flags_for_shell = pars_flags_for_shell[0][1]
            if __debug__:
                print('pars_flags_for_shell = ', pars_flags_for_shell,
                      '\ncommand =', command,
                      ' --- line 130 <class GITManager :: ->> pars_flags()')

            # look for values intersection in sets
            f_set = set(flags.values())
            p_f_for_shell = pars_flags_for_shell.intersection(f_set)
            if __debug__:
                print('flags.values() =', flags.values(),
                      'p_f_for_shell = ', p_f_for_shell,
                      ' --- line 138 <class GITManager :: ->> pars_flags()')

            # if there is not intersection raise KeyError
            if len(p_f_for_shell) == 0:
                # that one is just for situations like this $git help help or $ git help add
                if self._case_help(command, f_set):
                    p_f_for_shell = str(flags.popitem()[1])
                    return p_f_for_shell

                # if this is not the case then we gonna raise KeyError
                else:
                    try:
                        #file pattern case flag 'fp'
                        p_f_for_shell = flags['fp']

                        return p_f_for_shell
                    except KeyError:
                        raise KeyError("In %s \n f_set = %s is not in pars_flags_for_shell" % (self.pars_flags, f_set))
            # there is intersection
            else:
                # just one flag
                if len(p_f_for_shell) == 1:

                    # log --follow <filepattern>
                    if self._case_log_follow(p_f_for_shell):
                        try:
                            p_f_for_shell = list(p_f_for_shell)
                            p_f_for_shell.append(flags['fp'])
                            return p_f_for_shell
                        except KeyError:
                            raise KeyError("In %s \n f_set = %s is not in pars_flags_for_shell" % (self.pars_flags,
                                                                                                   f_set))

                    return p_f_for_shell.pop()
                # list of flags
                else:
                    value = []
                    for i in p_f_for_shell:
                        value.append(i)

                    return value
        # there is not any flags
        else:
            pars_flags_for_shell = None

        return pars_flags_for_shell

if __name__ == '__main__':
    import os
    obj = GITManager(os.getcwd())
    print(obj.execute_command('help'))
    print(obj.execute_command('status', fl='-s'))
#@TODO: Uzupełnić dokumentacje klas i metod przerobić SPHINX