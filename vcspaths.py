#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  vcspaths.py
#
#   Module sets path to VCS repository Git.
#   Default set path to user home directory if not specified.
#   You can change path:
#   - by function @see: set_path(repopath)
#   - get current path by call method @see: get_path()
#
# Copyright 2014 gumbi <pracacp@gmaill.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
__version__ = 'beta.1.0.3'
__author__ = 'gumbicp'

import bpy
import os


def if_vcs_exist():
    """
    Function checks if  git is in folder
    @return: True/False -> there is / or not
    """
    #check is there the same name of folder like name repository ?

    if os.access(".git", os.F_OK):
        return True
    else:
        return False


def set_path(repopath, clone=False):
    """
    Setter for repository path.
    @raise ValueError: just warning
    @param repopath: path to repository
    @return: path or raise ValueError when repository is not under
             this path or you made mistake in path
    """
    if not repopath:
        repopath = os.path.expanduser('~')

    path = os.path.normpath(repopath)
    os.chdir(path)

    if clone:
        return path

    elif not (if_vcs_exist()):
        raise ValueError('In {} there is not any repository!'.format(repopath))

    return path


def set_path_for_clone(bpy_path, http):
    """

    """
    (address, folder_git) = os.path.split(http)
    (folder, git) = os.path.splitext(folder_git)
    home_user = os.path.join(os.path.expanduser('~'), bpy_path)
    bpy_path = os.path.join(home_user, folder)
    bpy.context.scene.vcs_path = bpy_path
    set_path(bpy_path)
