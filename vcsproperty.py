# -*- coding: utf-8 -*-
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#   vcsproperty.py
#
#
#   Panel in scene properties for VCS(Version Control System) Add.
#
# Copyright 2014 gumbi <pracacp@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
__version__ = "beta.1.0.3"
__author__ = 'gumbicp'

import bpy
from bpy.props import *


class VcsProperty(bpy.types.Property):
    """ Class of properties for VSC repository """

    def update_vcs_variable(self, context):
        #print(10*'*', '\n', 'Update VcsPropGroup = {0} :: {1}'.format(self, self.values()), 10*'*', '\n')
        pass

    bpy.types.Scene.vcs_repo = StringProperty(name='vcs_repo',
                                              description='type of vcs repository',
                                              default='',
                                              maxlen=4,
                                              update=update_vcs_variable,)

    bpy.types.Scene.vcs_http = StringProperty(name='vcs_http',
                                              description='type of vcs string http',
                                              default='',
                                              update=update_vcs_variable,)

    bpy.types.Scene.vcs_path = StringProperty(name='vcs_path',
                                              description='type of vcs string path',
                                              subtype='FILE_PATH',
                                              default='',
                                              update=update_vcs_variable,)

    bpy.types.Scene.vcs_path_blend_file = StringProperty(name='vcs_path_blend_file',
                                                         description='type of vcs string path blend file',
                                                         subtype='FILE_PATH',
                                                         default='',
                                                         update=update_vcs_variable,)

    bpy.types.Scene.vcs_clone = BoolProperty(name="vcs_clone",
                                             description="Clone repository Flag",
                                             default=False,
                                             update=update_vcs_variable)